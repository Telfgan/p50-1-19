def dialog_string():
    print("Подсчёт символов в строке (количество запятых, пробелов и общее количество
символов)")
    while True:
        text = input("Текст (0, чтобы вернуться): ")
        if (text == "0"): break
        result = analyze(text)
        print(f"Всего символов: {result.total}")
        print(f"Пробелов: {result.space}")
        print(f"Запятых: {result.comma}")      
@dataclass
class AnalyzeResult:
    total: int = 0
    space: int = 0
    comma: int = 0
def analyze(text):
    result = AnalyzeResult()
    for c in text:
        result.total += 1
        if c == " ": result.space += 1
        elif c == ",": result.comma += 1
    return result